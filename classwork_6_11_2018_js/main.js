// Task 1
// Переменная num может принимать 4 значения: 1, 2, 3 или 4. Если она имеет значение '1', то в переменную result запишем 'зима', если имеет значение '2' – 'весна' и так далее. Решите задачу через switch-case.</p>
// var result;
// var num = prompt('Vvedite znachenie');
// switch(num) {
//     case '1': result = 'Zima'; document.write(result);
//         break;
//
//     case '2': result = 'Vesna'; document.write(result);
//         break;
//
//     case '3': result = 'Leto'; document.write(result);
//         break;
//
//     case '4': result = 'Osen'; document.write(result);
//         break;
//
//     default: document.write('Owibka');
// }

// Task 2
// <p>В переменной day лежит какое-то число из интервала от 1 до 31. Определите в какую декаду месяца попадает это число (в первую, вторую или третью).</p>
// var day = prompt('Vvedite znachenie');
// if (day > 0 && day <= 10) {
//     document.write('1 dekada');
// } else if(day > 10 && day <= 20) {
//     document.write('2 dekada');
// } else if(day > 20 && day <= 31) {
//     document.write('3 dekada');
// } else {
//     document.write('Owibka');
// }

// Task 3
// <p>Если переменная a равна нулю или равна двум, то прибавьте к ней 7, иначе поделите ее на 10. Выведите новое значение переменной на экран.</p>
// var a = prompt('Vvedite znachenie');
// var result;
// if (a == 0 || a == 2) {
//     result = parseInt(a) + 7;
// } else {
//     result = parseInt(a) / 7;
// }
// alert(result);

// Task 4
// <p>В переменной min лежит число от 0 до 59. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую).</p>
// var min = prompt('Vvedite znachenie');
// if (min >= 0 && min <=15) {
//     document.write('1');
// } else if (min >= 15 && min <= 30) {
//     document.write('2');
// } else if (min >= 30 && min <= 45) {
//     document.write('3');
// } else if (min >= 45 && min <= 59) {
//     document.write('4');
// } else {
//     document.write('Owibka');
// }

// Task 5
// <p>Перепишите if..else с использованием нескольких операторов '?'.
//     Для читаемости – оформляйте код в несколько строк.</p>
// <pre>
// <code>
// var message;
//
// if (login == 'Вася') {
//     message = 'Привет';
// } else if (login == 'Директор') {
//     message = 'Здравствуйте';
// } else if (login == '') {
//     message = 'Нет логина';
// } else {
//     message = '';
// }
//
// </code>
// </pre>
//
// var login = prompt('Vvedite znachenie');
//
// var message = (login == 'Вася') ? 'Привет' :
//     (login == 'Директор') ? 'Здравствуйте!' :
//         (login == '') ? 'Нет логина!' :
//             'Сработал Елс';
//
// alert(message);

// Task 6
// <p>Напишите программу кофейная машина. Программа принимает монеты и готовит напиток (Кофе 25коп, капучино 50коп, чай 75коп) программа также выдает сдачу покупателю и выводит рисунок с готовым напитком</p>

// var drink = prompt('Vvedite drink');
// var coin = prompt('Vvedite coin');
// var change;
//
//
// if (drink == 'Кофе') {
//     if (coin >= 25) {
//         if (coin == 25) {
//             document.write('There is no change. Please take a Кофе!');
//         } else {
//             change = coin - 25;
//             document.write('Please keep a change ' + change + ". Please take a Кофе!");
//         }
//     }
// } else if (drink == 'Kапучино') {
//     if (coin >= 50) {
//         if (coin == 50) {
//             document.write('There is no change. Please take a Kапучино!');
//         } else {
//             change = coin - 50;
//             document.write('Please keep a change ' + change + ". Please take a Kапучино!");
//         }
//     }
// } else if (drink == 'Чай') {
//     if (coin >= 75) {
//         if (coin == 75) {
//             document.write('There is no change. Please take a Чай!');
//         } else {
//             change = coin - 75;
//             document.write('Please keep a change ' + change + ". Please take a Чай!");
//         }
//     }
// } else {
//     document.write('There is no drink');
// }