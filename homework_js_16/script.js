// Window onload
window.onload = function() {

    // Define Elems
    const bodyElem = document.getElementsByTagName('body')[0];
    const switcherTheme = document.getElementById('btn-change-theme');


    // Check for isset of dark theme
    if (localStorage.getItem('theme') == 'dark') {
        bodyElem.classList.add("dark-theme");
    }

    // Switcher logic
    switcherTheme.addEventListener("click", function() {
        if (localStorage.getItem('theme') == 'dark') {
            localStorage.removeItem('theme');
            bodyElem.classList.remove("dark-theme");
        } else {
            localStorage.setItem('theme', 'dark');
            bodyElem.classList.add("dark-theme");
        }
    })

}